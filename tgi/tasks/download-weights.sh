#!/bin/bash -l

model_id=$1
hf_cache_host=${2:-~/ptmp_link/tmp/hf}

mkdir -p $hf_cache_host

module load apptainer/1.2.2
apptainer exec \
  -B ./:"$HOME","$hf_cache_host":/data \
  tgi.sif text-generation-server download-weights "$model_id"