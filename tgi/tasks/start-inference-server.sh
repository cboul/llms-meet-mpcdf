#!/bin/bash -l

model_id=$1
port=${2:-9999}
hf_cache_host=${2:-~/ptmp_link/tmp/hf}

srun apptainer run \
  --nv \
  -B ./:"$HOME","$hf_cache_host":/data \
  tgi.sif --model-id="$model_id" --port=$port --quantize=awq > inference_server.log 2>&1 &

# wait for the server to come up
apptainer exec -B .:"$HOME" clients.sif python tasks/wait_for_server.py --url="http://localhost:$port"

