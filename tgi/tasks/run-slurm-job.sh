#!/bin/bash

job=${1:-run_llm_inference.slurm}

# Submit the job and capture the output
output=$(sbatch $job)
echo "$output"

# Extract the job ID using grep and awk
job_id=$(echo $output | grep -oP 'Submitted batch job \K\d+')

# Check if we successfully extracted a job ID
if [[ -z $job_id ]]; then
    echo "Failed to submit job: $output"
    exit 1
fi

# Monitor the queue for the specific job ID
while squeue --me | grep -q "$job_id"; do
    echo -n "."
    sleep 1
done
echo "\nJob $job_id completed."

# Check for job output and error files
output_file="joblogs/job.out.$job_id"
error_file="joblogs/job.err.$job_id"

if [[ -f $output_file ]]; then
    echo "Output from job $job_id:"
    cat $output_file
else
    echo "No output file found for job $job_id."
fi

if [[ -f $error_file ]]; then
    echo "Errors were encountered during the running of job $job_id. See $error_file for details."
fi
