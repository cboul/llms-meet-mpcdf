#!/bin/bash

printf "Messages API (compatible with OpenAI Chat Completion API)\n"
curl localhost:9999/v1/chat/completions \
    -X POST \
    -d '{
  "model": "tgi",
  "messages": [
    {
      "role": "user",
      "content": "What is deep learning?"
    }
  ],
  "stream": false,
  "max_tokens": 20
}' \
    -H 'Content-Type: application/json'
exit 0

curl localhost:9999/v1/chat/completions \
    -X POST \
    -d '{
  "model": "tgi",
  "messages": [
    {
      "role": "system",
      "content": "You are a helpful assistant."
    },
    {
      "role": "user",
      "content": "What is deep learning?"
    }
  ],
  "stream": false,
  "max_tokens": 20
}' \
    -H 'Content-Type: application/json'
	
printf "\nTGI API\n"

curl localhost:9999/generate \
    -X POST \
    -d '{"inputs":"What is Deep Learning?","parameters":{"max_new_tokens":20}}' \
    -H 'Content-Type: application/json'

printf "\n"
