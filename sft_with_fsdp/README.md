# Supervised-Fine-Tuning with TRL and FSDP

> "TRL is a full stack library where we provide a set of tools to train transformer language models with Reinforcement Learning, from the Supervised Fine-tuning step (SFT), Reward Modeling step (RM) to the Proximal Policy Optimization (PPO) step. The library is integrated with 🤗 transformers."
> 
> *https://huggingface.co/docs/trl/index*

In this example we use TRL to supervise-fine-tune a [Llama2 70B model](https://huggingface.co/meta-llama/Llama-2-70b-hf) via [FSDP](https://pytorch.org/docs/stable/fsdp.html).
We adapted the example from this [blog post by Hugging Face](https://huggingface.co/blog/ram-efficient-pytorch-fsdp). 

## Build environment/containers
We use the official NVIDIA Pytorch docker image as a base, and create our `trl.sif` container on top of it.
```shell
module load apptainer/1.2.2
apptainer build --fakeroot nvidia_pytorch.sif nvidida_pytorch.def
apptainer build --fakeroot trl.sif trl.def
```

## Download model and data
First download the data and the pre-trained model weights:
```shell
mkdir -p /ptmp/${USER}/huggingface
mkdir -p /ptmp/${USER}/dataset
apptainer exec \
    -B .:"$HOME",/ptmp/$USER \
    --env "HF_HOME=/ptmp/${USER}/huggingface" \
    trl.sif python download_model_and_data.py \
        --model_name="meta-llama/Llama-2-70b-hf" \
        --dataset_path="/ptmp/${USER}/dataset" \
        --hf_token="..."
```

## Submit training job
The default settings in the slurm script runs a training on 8 nodes, on a total of 32 GPUs.
It will run the training for 500 steps, which should take less than 3 hours.

```sehll
sbatch train.slurm
```

> :warning: Be careful when using [datasets](https://huggingface.co/docs/datasets/index) in your training script! 
> Their lazy caching mechanism can cause IO issues on our GPFS filesystem when multiple nodes try to do the caching at the same location.
> By setting the env variable `HF_DATASETS_CACHE=$JOB_SHMTMPDIR`, for example, we make sure that each node does the caching separately on its CPU RAM.
> This is also more performant.

## Track training run
We use [WandB](https://wandb.ai/) to track our training run. 
Since it is not allowed to connect to the Internet from our compute nodes, we use it in [offline mode](https://docs.wandb.ai/guides/technical-faq/setup#can-i-run-wandb-offline).
To upload the status of your training run to WandB, simply run:
```shell
apptainer exec trl.sif wandb sync wandb/latest-run
```
