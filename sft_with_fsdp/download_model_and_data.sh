#!/bin/bash
module load apptainer/1.2.2
mkdir -p /ptmp/${USER}/huggingface
mkdir -p /ptmp/${USER}/dataset
apptainer exec \
    -B .:"$HOME",/ptmp/$USER \
    --env "HF_HOME=/ptmp/${USER}/huggingface" \
    trl.sif python download_model_and_data.py \
        --model_name="meta-llama/Llama-2-70b-hf" \
        --dataset_path="/ptmp/${USER}/dataset"

